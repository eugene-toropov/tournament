package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"
	"tournament/config"
	"tournament/log"

	"tournament/server"

	"github.com/spf13/viper"
)

func main() {
	// Read config
	config.Init()
	// Init logger
	logger := log.Get()
	// Catch signal for graceful shutdown
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	// Create new server instance
	server := server.New()
	httpServer := &http.Server{Addr: viper.GetString("addr"), Handler: server.HTTPHandler()}
	// Run it
	go func() {
		logger.Infof("Listening on %s", viper.GetString("addr"))
		if err := httpServer.ListenAndServe(); err != nil {
			logger.Fatal(err)
		}
	}()
	// Handle graceful shutdown
	<-stop
	logger.Info("Shutting down the server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	httpServer.Shutdown(ctx)
	logger.Info("Server gracefully stopped")
}
