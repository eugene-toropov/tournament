package locks

import "sync"

// TournamentLocks is a set of tournament locks to prevent race conditions
type TournamentLocks map[int]*sync.RWMutex

// Lock locks a tournament
func (gl TournamentLocks) Lock(tournamentID int) {
	if _, exists := gl[tournamentID]; !exists {
		gl[tournamentID] = &sync.RWMutex{}
	}
	gl[tournamentID].Lock()
	return
}

// Unlock unlocks a tournament
func (gl TournamentLocks) Unlock(tournamentID int) {
	// Nothing to unlock
	if _, exists := gl[tournamentID]; !exists {
		return
	}
	gl[tournamentID].Unlock()
	return
}

// RLock locks a tournament for reading
func (gl TournamentLocks) RLock(tournamentID int) {
	if _, exists := gl[tournamentID]; !exists {
		gl[tournamentID] = &sync.RWMutex{}
	}
	gl[tournamentID].RLock()
	return
}

// RUnlock unlocks a tournament for reading
func (gl TournamentLocks) RUnlock(tournamentID int) {
	if _, exists := gl[tournamentID]; !exists {
		gl[tournamentID] = &sync.RWMutex{}
	}
	gl[tournamentID].RUnlock()
	return
}

// Tournaments is a global map of tournament locks to prevent race conditions
var Tournaments = TournamentLocks{}
