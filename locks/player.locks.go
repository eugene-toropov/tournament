package locks

import "sync"

// PlayerLocks is a set of player locks to prevent player race conditions
type PlayerLocks map[string]*sync.RWMutex

// Lock locks a player
func (pl PlayerLocks) Lock(playerID string) {
	if _, exists := pl[playerID]; !exists {
		pl[playerID] = &sync.RWMutex{}
	}
	pl[playerID].Lock()
	return
}

// Unlock unlocks a player
func (pl PlayerLocks) Unlock(playerID string) {
	// Nothing to unlock
	if _, exists := pl[playerID]; !exists {
		return
	}
	pl[playerID].Unlock()
	return
}

// RLock locks a player for reading
func (pl PlayerLocks) RLock(playerID string) {
	// Nothing to unlock
	if _, exists := pl[playerID]; !exists {
		return
	}
	pl[playerID].RLock()
	return
}

// RUnlock unlocks a player for reading
func (pl PlayerLocks) RUnlock(playerID string) {
	// Nothing to unlock
	if _, exists := pl[playerID]; !exists {
		return
	}
	pl[playerID].RUnlock()
	return
}

// Players is a global map of player locks to prevent player race conditions
var Players = PlayerLocks{}
