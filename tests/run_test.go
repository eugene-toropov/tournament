package tests

import (
	"net/http/httptest"
	"testing"
	"tournament/config"
	"tournament/server"

	"github.com/gavv/httpexpect"
)

// TestAll runs all tests
func TestAll(t *testing.T) {
	// Read config
	config.Init()
	// Create new server instance
	s := server.New()

	server := httptest.NewServer(s.HTTPHandler())
	defer server.Close()

	e := httpexpect.New(t, server.URL)

	// Test /ok route
	TestOKAPI(e)
	// Test Player API
	TestPlayerAPI(e)
	// Test Tournament API
	TestTournamentAPI(e)
}
