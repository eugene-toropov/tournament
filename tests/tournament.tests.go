package tests

import (
	"net/http"
	"tournament/log"

	"github.com/gavv/httpexpect"
)

// TestTournamentAPI tests tournament API
func TestTournamentAPI(e *httpexpect.Expect) {
	logger := log.Get()

	if e == nil {
		logger.Error("httpexpect.Expect object is not defined")
	}

	// Create tournament 1
	e.GET("/announceTournament").
		WithQuery("tournamentId", 1).
		WithQuery("deposit", 1000).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", 1).
		ContainsKey("deposit").ValueEqual("deposit", 1000)

	// Create tournament 2
	e.GET("/announceTournament").
		WithQuery("tournamentId", 2).
		WithQuery("deposit", 2000).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", 2).
		ContainsKey("deposit").ValueEqual("deposit", 2000)

	// Player P5 joins tournament 1
	responseJSON := e.GET("/joinTournament").
		WithQuery("tournamentId", 1).
		WithQuery("playerId", "P5").
		Expect().
		Status(http.StatusOK).
		JSON().Object()

	responseJSON.ContainsKey("tournament").Value("tournament").Object().
		ContainsKey("id").ValueEqual("id", 1)

	responseJSON.ContainsKey("player").Value("player").Object().
		ContainsKey("id").ValueEqual("id", "P5")

	// Players P1, P2, P3, P4 join tournament 1
	responseJSON = e.GET("/joinTournament").
		WithQuery("tournamentId", 1).
		WithQuery("playerId", "P1").
		WithQuery("backerId", "P2").
		WithQuery("backerId", "P3").
		WithQuery("backerId", "P4").
		Expect().
		Status(http.StatusOK).
		JSON().Object()

	responseJSON.ContainsKey("tournament").Value("tournament").Object().
		ContainsKey("id").ValueEqual("id", 1)

	responseJSON.ContainsKey("player").Value("player").Object().
		ContainsKey("id").ValueEqual("id", "P1")

	responseJSON.ContainsKey("backers").Value("backers").Array().ContainsOnly("P2", "P3", "P4")

	// Finish tournament 1
	e.POST("/resultTournament").
		WithBytes([]byte(`
			{
			  "tournamentId": 1,
			  "winners": [
			    {
			      "playerId": "P1",
			      "prize": 2000
			    }
			  ]
			}
		`)).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("ok").ValueEqual("ok", true)
}
