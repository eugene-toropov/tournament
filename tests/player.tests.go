package tests

import (
	"net/http"
	"tournament/log"

	"github.com/gavv/httpexpect"
)

// TestPlayerAPI tests player API
func TestPlayerAPI(e *httpexpect.Expect) {
	logger := log.Get()

	if e == nil {
		logger.Error("httpexpect.Expect object is not defined")
	}

	e.GET("/fund").
		WithQuery("playerId", "P1").
		WithQuery("points", 300).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", "P1").
		ContainsKey("points").ValueEqual("points", 300)

	e.GET("/fund").
		WithQuery("playerId", "P2").
		WithQuery("points", 300).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", "P2").
		ContainsKey("points").ValueEqual("points", 300)

	e.GET("/fund").
		WithQuery("playerId", "P3").
		WithQuery("points", 300).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", "P3").
		ContainsKey("points").ValueEqual("points", 300)

	e.GET("/fund").
		WithQuery("playerId", "P4").
		WithQuery("points", 500).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", "P4").
		ContainsKey("points").ValueEqual("points", 500)

	e.GET("/fund").
		WithQuery("playerId", "P5").
		WithQuery("points", 1000).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ContainsKey("id").ValueEqual("id", "P5").
		ContainsKey("points").ValueEqual("points", 1000)

}
