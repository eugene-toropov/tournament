package tests

import (
	"net/http"
	"tournament/log"

	"github.com/gavv/httpexpect"
)

// TestOKAPI tests /ok route
func TestOKAPI(e *httpexpect.Expect) {
	logger := log.Get()

	if e == nil {
		logger.Error("httpexpect.Expect object is not defined")
	}

	e.GET("/ok").
		Expect().
		Status(http.StatusOK).
		JSON().Object().ContainsKey("ok").ValueEqual("ok", true)

}
