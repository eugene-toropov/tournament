package mysql

import (
	"sync"
	"tournament/log"

	_ "github.com/go-sql-driver/mysql" // mysql driver
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

// MySQL is a MySQL DB handler
type MySQL struct {
	*sqlx.DB
}

var dbm MySQL
var once sync.Once

// Dbh returns MySQL db handler
func Dbh() MySQL {
	once.Do(func() {
		logger := log.Get()

		dbUser := viper.GetString("mysql_user")
		dbPass := viper.GetString("mysql_pass")
		dbHost := viper.GetString("mysql_host")
		dbPort := viper.GetString("mysql_port")
		dbName := viper.GetString("mysql_name")

		if dbUser == "" || dbHost == "" {
			logger.Fatal("MySQL Host or User are not defined")
		}

		db, err := sqlx.Open("mysql", dbUser+":"+dbPass+"@tcp("+dbHost+":"+dbPort+")/"+dbName+"?charset=utf8&parseTime=True&loc=Local")
		if err != nil {
			logger.Fatalf("sqlx.Open failed: %s", err)
		}

		if err := db.Ping(); err != nil {
			logger.Fatal(err)
		}

		dbm = MySQL{db}
	})

	return dbm
}
