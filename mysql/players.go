package mysql

import (
	"database/sql"
	"tournament/locks"
	"tournament/log"
	"tournament/types"
)

// GetPlayer returns player by ID
func GetPlayer(playerID string, lock bool) *types.Player {
	if playerID == "" {
		return nil
	}
	logger := log.Get()
	// Lock the player
	if lock {
		locks.Players.RLock(playerID)
		defer locks.Players.RUnlock(playerID)
	}
	query := `
select
	id,
	points
from
	players
where
	id = ?
	`
	player := types.Player{}
	row := Dbh().QueryRowx(query, playerID)
	if err := row.StructScan(&player); err != nil && err != sql.ErrNoRows {
		logger.Error(err)
	}
	if player.ID == "" {
		return nil
	}
	return &player
}

// GetPlayerDeposit returns total points deposited by plaer on all tournaments
func GetPlayerDeposit(playerID string, lock bool) int64 {
	if playerID == "" {
		return 0
	}
	logger := log.Get()
	// Lock the player
	if lock {
		locks.Players.RLock(playerID)
		defer locks.Players.RUnlock(playerID)
	}
	query := `
select
	sum(deposit)
from
	tournament_players
where
	player_id = ?
	`
	var total sql.NullInt64
	err := Dbh().Get(&total, query, playerID)
	if err != nil {
		logger.Error(err)
	}
	return total.Int64
}

// CreatePlayer creates new player
func CreatePlayer(playerID string, points int) error {
	logger := log.Get()
	// Run MySQL query
	_, err := Dbh().Exec("insert into players set id = ?, points = ?", playerID, points)
	logger.Debugf("Created player %s, points - %d", playerID, points)
	return err
}

// AddPlayerPoints funds player with additional points
func AddPlayerPoints(playerID string, points int, lock bool) error {
	logger := log.Get()
	// Lock the player
	if lock {
		locks.Players.Lock(playerID)
		defer locks.Players.Unlock(playerID)
	}
	// Run MySQL query
	_, err := Dbh().Exec("update players set points = points + ? where id = ?", points, playerID)
	logger.Debugf("Added %d points for player %s", points, playerID)
	return err
}
