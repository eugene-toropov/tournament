package mysql

import (
	"database/sql"
	"strings"
	"tournament/locks"
	"tournament/log"
	"tournament/types"
)

// GetTournament returns tournament by ID
func GetTournament(tournamentID int, lock bool) *types.Tournament {
	if tournamentID == 0 {
		return nil
	}
	logger := log.Get()
	if lock {
		locks.Tournaments.RLock(tournamentID)
		defer locks.Tournaments.RUnlock(tournamentID)
	}
	query := `
select
	id,
	deposit
from
	tournaments
where
	id = ?
	`
	tournament := types.Tournament{}
	row := Dbh().QueryRowx(query, tournamentID)
	if err := row.StructScan(&tournament); err != nil && err != sql.ErrNoRows {
		logger.Error(err)
	}
	if tournament.ID == 0 {
		return nil
	}
	return &tournament
}

// CreateTournament creates new tournament
func CreateTournament(tournamentID int, deposit int) error {
	logger := log.Get()
	_, err := Dbh().Exec("insert into tournaments set id = ?, deposit = ?", tournamentID, deposit)
	logger.Debugf("Created tournament %d, deposit - %d", tournamentID, deposit)
	return err
}

// JoinTournament adds new player to a tournament with a certain deposit
func JoinTournament(tournamentID int, playerID string, backerIDs []string, deposit int) error {
	logger := log.Get()
	_, err := Dbh().Exec("replace into tournament_players set tournament_id = ?, player_id = ?, backer_ids = ?, team_size = ?, deposit = ?", tournamentID, playerID, strings.Join(backerIDs, ","), len(backerIDs)+1, deposit)
	if len(backerIDs) > 0 {
		logger.Debugf("Player %s has joined tournament %d supported by backers %s with a deposit of %d", playerID, tournamentID, strings.Join(backerIDs, ", "), deposit)
	} else {
		logger.Debugf("Player %s has joined tournament %d with a deposit of %d", playerID, tournamentID, deposit)
	}
	return err
}

// SetTournamentDeposit sets tournament deposit
func SetTournamentDeposit(tournamentID int, deposit int) error {
	logger := log.Get()
	_, err := Dbh().Exec("update tournaments set deposit = ? where id = ?", deposit, tournamentID)
	logger.Debugf("Updated tournament %d with a deposit of %d", tournamentID, deposit)
	return err
}

// GetTournamentPlayers returns tournament players
func GetTournamentPlayers(tournamentID int, lock bool) []*types.TournamentPlayer {
	if tournamentID == 0 {
		return nil
	}
	logger := log.Get()
	if lock {
		locks.Tournaments.RLock(tournamentID)
		defer locks.Tournaments.RUnlock(tournamentID)
	}
	query := `
select
	tournament_id,
	player_id,
	backer_ids,
	team_size,
	deposit
from
	tournament_players
where
	tournament_id = ?
	`
	players := []*types.TournamentPlayer{}
	err := Dbh().Select(&players, query, tournamentID)
	if err != nil {
		logger.Error(err)
		return nil
	}
	return players
}

// GetTournamentPrize returns tournament prize
func GetTournamentPrize(tournamentID int, lock bool) int64 {
	if tournamentID == 0 {
		return 0
	}
	logger := log.Get()
	if lock {
		locks.Tournaments.RLock(tournamentID)
		defer locks.Tournaments.RUnlock(tournamentID)
	}
	query := `
select
	sum(deposit)
from
	tournament_players
where
	tournament_id = ?
	`
	var total sql.NullInt64
	err := Dbh().Get(&total, query, tournamentID)
	if err != nil {
		logger.Error(err)
	}
	return total.Int64
}

// FinishTournament sets player points for all provided players once tournament has finished + removes all tournament players
// TODO: run in transaction
func FinishTournament(tournamentID int, winPoints map[string]int) error {
	if winPoints == nil {
		return nil
	}
	// Update player points
	for playerID, points := range winPoints {
		err := AddPlayerPoints(playerID, points, false)
		if err != nil {
			return err
		}
	}
	_, err := Dbh().Exec("delete from tournament_players where tournament_id = ?", tournamentID)
	if err != nil {
		return err
	}
	return nil
}
