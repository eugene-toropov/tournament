package server

import (
	"net/http"

	"gopkg.in/matryer/respond.v1"
)

// mapRoutes defines all the routes the server has
func (s *Server) mapRoutes() {
	// Handle standard ACK with GET /ok
	s.handleFunc("/ok", func(w http.ResponseWriter, r *http.Request) { respond.With(w, r, http.StatusOK, ok) }).Methods(GET)
	// Fund player account with points. If no player exists - create new player
	s.handleFunc("/fund", s.handleFundPlayer).Methods("GET")
	// Announce Tournament with a certain deposit
	s.handleFunc("/announceTournament", s.handleAnnounceTournament).Methods("GET")
	// Player joins a tournament (with an optional set of backers)
	s.handleFunc("/joinTournament", s.handleJoinTournament).Methods("GET")
	// Result tournament winners and prizes
	s.handleFunc("/resultTournament", s.handleResultTournament).Methods("POST")
}
