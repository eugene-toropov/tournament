package server

import (
	"strings"
	"tournament/log"

	respond "gopkg.in/matryer/respond.v1"

	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

// CORS Configuration
var apiCORS = cors.New(cors.Options{
	AllowedHeaders:     []string{"Accept", "Content-Type", "Content-Length", "X-CRSF-Token", "Authorization", "Cache-Control", "If-Modified-Since", "Pragma", "X-Total-Count"},
	AllowedMethods:     []string{"POST", "GET", "PUT", "OPTIONS", "DELETE"},
	ExposedHeaders:     []string{"X-Total-Count"},
	AllowCredentials:   true,
	OptionsPassthrough: true,
})

// Server is a server
type Server struct {
	logger log.Logger
	router *mux.Router
}

// New creates new server instance
func New() *Server {
	s := &Server{
		logger: log.Get(),
		router: mux.NewRouter(),
	}
	// Map server routes
	s.mapRoutes()
	return s
}

// HTTPHandler gets the http.Handler for this Server
func (s *Server) HTTPHandler() http.Handler {
	return apiCORS.Handler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Default Secure Headers
		// See https://scotthelme.co.uk/hardening-your-http-response-headers/
		w.Header().Set("Strict-Transport-Security", "max-age=31536000; includeSubDomains")
		w.Header().Set("X-Frame-Options", "DENY")
		w.Header().Set("X-Xss-Protection", "1; mode=block")
		// Breaks IE11 handler when set - header is removed in api.image.handler.go writeImage and writeRaw
		w.Header().Set("X-Content-Type-Options", "nosniff")

		// If it's OPTIONS just allow and retun HTTP Status OK (for CORS)
		if strings.ToUpper(r.Method) == OPTIONS {
			respond.With(w, r, http.StatusOK, ok)
			return
		}

		s.router.ServeHTTP(w, r)
	}))
}

// ServeHTTP
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

// handleFunc passes the requested path to the correct router handler
func (s *Server) handleFunc(path string, fn http.HandlerFunc) *mux.Route {
	return s.router.HandleFunc("/"+strings.TrimLeft(path, "/"), fn)
}
