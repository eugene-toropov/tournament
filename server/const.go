package server

// ok is a standard ACK response when requesting [API]/ok with a GET verb
var ok = map[string]interface{}{"ok": true}

// Supported HTTP Methods for the server
const (
	POST    = "POST"
	GET     = "GET"
	PUT     = "PUT"
	OPTIONS = "OPTIONS"
	DELETE  = "DELETE"
)

// UseLock is a constant to use lock
const UseLock = true

// SkipLock is a constant to skip lock
const SkipLock = false
