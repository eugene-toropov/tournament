package server

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"tournament/locks"
	"tournament/mysql"
	"tournament/types"

	respond "gopkg.in/matryer/respond.v1"
)

// handleAnnounceTournament announces Tournament with a certain deposit
func (s *Server) handleAnnounceTournament(w http.ResponseWriter, r *http.Request) {
	s.logger.Debug("handleAnnounceTournament")
	// Get query string
	q := r.URL.Query()
	// Validate tournament ID
	qTournamentID := q.Get("tournamentId")
	if qTournamentID == "" {
		s.logger.Error("No tournament ID provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("tournamentId"))
		return
	}
	tournamentID, err := strconv.Atoi(qTournamentID)
	if err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	// Validate deposit
	qDeposit := q.Get("deposit")
	if qDeposit == "" {
		s.logger.Error("No deposit provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("deposit"))
		return
	}
	deposit, err := strconv.Atoi(qDeposit)
	if err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	if deposit <= 0 {
		err := errors.New("Deposit can't be negative or zero")
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	// If tournament exists - update deposit
	tournament := mysql.GetTournament(tournamentID, UseLock)
	if tournament != nil {
		if err := mysql.SetTournamentDeposit(tournamentID, deposit); err != nil {
			s.logger.Error(err)
			respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
			return
		}
	} else {
		// Tournament doesn't exist - create it with provided deposit
		if err := mysql.CreateTournament(tournamentID, deposit); err != nil {
			s.logger.Error(err)
			respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
			return
		}
	}

	// Get created/updated tournament
	tournament = mysql.GetTournament(tournamentID, UseLock)

	respond.With(w, r, http.StatusOK, tournament)
}

// handleJoinTournament handles player joining a tournament with an optional set of backers
func (s *Server) handleJoinTournament(w http.ResponseWriter, r *http.Request) {
	s.logger.Debug("handleJoinTournament")
	// Get query string
	q := r.URL.Query()
	// Validate tournament ID
	qTournamentID := q.Get("tournamentId")
	if qTournamentID == "" {
		s.logger.Error("No tournament ID provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("tournamentId"))
		return
	}
	tournamentID, err := strconv.Atoi(qTournamentID)
	if err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	// Check tournament exists
	tournament := mysql.GetTournament(tournamentID, UseLock)
	if tournament == nil {
		e := fmt.Errorf("Tournament %d doesn't exist", tournamentID)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Validate player ID
	playerID := q.Get("playerId")
	if playerID == "" {
		s.logger.Error("No player ID provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("playerId"))
		return
	}
	// Lock the player
	locks.Players.Lock(playerID)
	defer locks.Players.Unlock(playerID)
	// Check player exists
	player := mysql.GetPlayer(playerID, SkipLock)
	if player == nil {
		e := fmt.Errorf("Player %s doesn't exist", playerID)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Validate optional backer IDs
	backerIDs := q["backerId"]
	// Also count total points players have...
	allPlayersPoints := player.Points
	// ... and count deposit players spent having joined other tournaments
	allPlayersDeposit := mysql.GetPlayerDeposit(playerID, SkipLock)
	// allPlayersDeposit
	allPlayerIDs := []string{playerID}
	if len(backerIDs) > 0 {
		for _, backerID := range backerIDs {
			// Lock the backer
			locks.Players.Lock(backerID)
			defer locks.Players.Unlock(backerID)
			// Check backer exists
			backer := mysql.GetPlayer(backerID, SkipLock)
			if backer == nil {
				e := fmt.Errorf("Backer %s doesn't exist", backerID)
				s.logger.Error(e)
				respond.With(w, r, http.StatusBadRequest, errorAsString(e))
				return
			}
			allPlayersPoints += backer.Points
			allPlayerIDs = append(allPlayerIDs, backerID)
			allPlayersDeposit += mysql.GetPlayerDeposit(backerID, SkipLock)
		}
	}
	// Check player and backers have enough points
	if int64(allPlayersPoints)-allPlayersDeposit < int64(tournament.Deposit) {
		e := fmt.Errorf("Player %s and backers %s don't have enough points (Points - %d. Deposit - %d) to join tournament %d with deposit of %d", playerID, strings.Join(backerIDs, ", "), allPlayersPoints, allPlayersDeposit, tournament.ID, tournament.Deposit)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Count win points for every player
	winPointsStr := fmt.Sprintf("%d", tournament.Deposit/len(allPlayerIDs))
	winPoints, err := strconv.Atoi(winPointsStr)
	if err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	// Lock the tournament
	locks.Tournaments.Lock(tournamentID)
	defer locks.Tournaments.Unlock(tournamentID)

	// Add tournament player
	if err := mysql.JoinTournament(tournamentID, playerID, backerIDs, winPoints); err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}

	respond.With(w, r, http.StatusOK, map[string]interface{}{
		"tournament": tournament,
		"player":     player,
		"backers":    backerIDs,
	})
}

// handleResultTournament handles tournament result
func (s *Server) handleResultTournament(w http.ResponseWriter, r *http.Request) {
	s.logger.Debug("handleResultTournament")
	// Parse JSON request
	var resultReq types.ResultTournamentRequest
	defer r.Body.Close()
	if err := unmarshal(r.Body, &resultReq); err != nil {
		s.logger.Error("Unable to decode payload:", err)
		respond.With(w, r, http.StatusBadRequest, stringAsErrorString("Unable to decode payload"))
		return
	}
	// Lock the tournament
	locks.Tournaments.Lock(resultReq.TournamentID)
	defer locks.Tournaments.Unlock(resultReq.TournamentID)
	// Validate tournament
	tournament := mysql.GetTournament(resultReq.TournamentID, SkipLock)
	if tournament == nil {
		e := fmt.Errorf("Tournament %d doesn't exist", resultReq.TournamentID)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Get tournament players
	tournamentPlayers := mysql.GetTournamentPlayers(resultReq.TournamentID, SkipLock)
	if len(tournamentPlayers) == 0 {
		e := fmt.Errorf("Tournament %d has no players", resultReq.TournamentID)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Count points they lose as tournament has ended
	winPoints := map[string]int{}
	tournamentPlayersIdx := map[string]*types.TournamentPlayer{}
	for _, player := range tournamentPlayers {
		winPoints[player.PlayerID] = -player.Deposit
		for _, backerID := range strings.Split(player.BackerIDs, ",") {
			if backerID == "" {
				continue
			}
			winPoints[backerID] = -player.Deposit
		}
		tournamentPlayersIdx[player.PlayerID] = player
	}
	// Get tournament prize
	tournamentPrize := mysql.GetTournamentPrize(resultReq.TournamentID, SkipLock)
	// Make sure overall posted prize is lower than tournament prize
	var allWinnersPrize int64
	for _, winner := range resultReq.Winners {
		allWinnersPrize += int64(winner.Prize)
	}
	if tournamentPrize > allWinnersPrize {
		e := fmt.Errorf("Tournament %d prize (%d) is lower than overall winners' prize (%d)", resultReq.TournamentID, tournamentPrize, allWinnersPrize)
		s.logger.Error(e)
		respond.With(w, r, http.StatusBadRequest, errorAsString(e))
		return
	}
	// Handle winners
	for _, winner := range resultReq.Winners {
		// Lock the winner
		locks.Players.Lock(winner.PlayerID)
		defer locks.Players.Unlock(winner.PlayerID)
		// Check winner exists
		player := mysql.GetPlayer(winner.PlayerID, SkipLock)
		if player == nil {
			e := fmt.Errorf("Player %s doesn't exist", winner.PlayerID)
			s.logger.Error(e)
			respond.With(w, r, http.StatusBadRequest, errorAsString(e))
			return
		}
		// Check winner takes part in the tournament
		tournamentPlayer, exists := tournamentPlayersIdx[winner.PlayerID]
		if !exists {
			e := fmt.Errorf("Winner %s doesn't take part in the tournament %d", winner.PlayerID, tournament.ID)
			s.logger.Error(e)
			respond.With(w, r, http.StatusBadRequest, errorAsString(e))
			return
		}
		// Count points winner's team gains
		pointsStr := fmt.Sprintf("%d", winner.Prize/tournamentPlayer.TeamSize)
		points, err := strconv.Atoi(pointsStr)
		if err != nil {
			s.logger.Error(err)
			respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
			return
		}
		// Add points for winner and their backers
		winPoints[winner.PlayerID] += points
		for _, backerID := range strings.Split(tournamentPlayer.BackerIDs, ",") {
			winPoints[backerID] += points
		}
	}
	// Finish tournament
	if err := mysql.FinishTournament(tournament.ID, winPoints); err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	respond.With(w, r, http.StatusOK, ok)
}
