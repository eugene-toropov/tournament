package server

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// unmarshal unmarshalles a Reader Object and returns as a mapped string interface the JSON document
// See https://ahmetalpbalkan.com/blog/golang-json-decoder-pitfalls/ for why to change from decode.
func unmarshal(r io.Reader, obj interface{}) error {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, obj)
}
