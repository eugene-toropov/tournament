package server

import (
	"errors"
	"net/http"
	"strconv"
	"tournament/mysql"

	respond "gopkg.in/matryer/respond.v1"
)

// handleFundPlayer funds player account with points. If no player exists - create new player
func (s *Server) handleFundPlayer(w http.ResponseWriter, r *http.Request) {
	s.logger.Debug("handleFundPlayer")
	// Get query string
	q := r.URL.Query()
	// Validate player ID
	playerID := q.Get("playerId")
	if playerID == "" {
		s.logger.Error("No player ID provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("playerId"))
		return
	}
	// Validate points
	qPoints := q.Get("points")
	if qPoints == "" {
		s.logger.Error("No points provided")
		respond.With(w, r, http.StatusBadRequest, errorMissingParameter("points"))
		return
	}
	points, err := strconv.Atoi(qPoints)
	if err != nil {
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	if points <= 0 {
		err := errors.New("Points can't be negative or zero")
		s.logger.Error(err)
		respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
		return
	}
	// Find player by ID
	player := mysql.GetPlayer(playerID, UseLock)
	// Create player if missing with provided points
	if player == nil {
		if err := mysql.CreatePlayer(playerID, points); err != nil {
			s.logger.Error(err)
			respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
			return
		}
	} else {
		// Fund player with points
		if err := mysql.AddPlayerPoints(playerID, points, UseLock); err != nil {
			s.logger.Error(err)
			respond.With(w, r, http.StatusInternalServerError, errorAsString(err))
			return
		}
	}
	// Get updated player info
	player = mysql.GetPlayer(playerID, UseLock)

	respond.With(w, r, http.StatusOK, player)
}
