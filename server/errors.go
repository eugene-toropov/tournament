package server

// Error is a "constant error" implementation as per https://dave.cheney.net/2016/04/07/constant-errors
type Error string

func (e Error) Error() string { return string(e) }

// 404 Not Found
const err404NotFound = Error("Not found")

// 403 Forbidden
const err403Forbidden = Error("Forbidden")

// 400 Bad Request
const errBadRequest = Error("Bad request")

// 500 Internal Server Error
var err500InternalServerError = Error("Internal server error")

// errorAsString maps an error object to a standard JSON error response string
func errorAsString(e error) map[string]interface{} {
	return map[string]interface{}{"error": e.Error()}
}

// errorMissingParameter creates a standard error response inserting the parameter name into the return and mapping into a JSON error response string
func errorMissingParameter(s string) map[string]interface{} {
	return map[string]interface{}{"error": "Missing " + s + " parameter"}
}

// stringAsErrorString maps a string to a standard JSON error response string
func stringAsErrorString(s string) map[string]interface{} {
	return map[string]interface{}{"error": s}
}
