package types

// Player is a player :)
type Player struct {
	ID     string `db:"id" json:"id"`
	Points int    `db:"points" json:"points"`
}
