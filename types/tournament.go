package types

// Tournament is a tournament :)
type Tournament struct {
	ID      int `db:"id" json:"id"`
	Deposit int `db:"deposit" json:"deposit"`
}

// TournamentPlayer is a tournament player
type TournamentPlayer struct {
	TournamentID int    `db:"tournament_id"`
	PlayerID     string `db:"player_id"`
	BackerIDs    string `db:"backer_ids"`
	TeamSize     int    `db:"team_size"`
	Deposit      int    `db:"deposit"`
}

// ResultTournamentRequest is a JSON request to finish tournament
type ResultTournamentRequest struct {
	TournamentID int                             `json:"tournamentId"`
	Winners      []ResultTournamentRequestWinner `json:"winners"`
}

// ResultTournamentRequestWinner is a tournament winner
type ResultTournamentRequestWinner struct {
	PlayerID string `json:"playerId"`
	Prize    int    `json:"prize"`
}
