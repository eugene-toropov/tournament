package config

import (
	"fmt"
	"path"
	"runtime"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

// Init reads into viper
func Init() {
	// Set config file name
	viper.SetConfigName("config")
	// Set config path = current package path
	_, filename, _, _ := runtime.Caller(0)
	viper.AddConfigPath(path.Dir(filename))
	// Read config
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	// Reload config on change
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	return
}
